package com.doapps.android.domain

import android.os.Build
import com.doapps.android.domain.repo.CurrentWeatherRepositoryTest
import com.doapps.android.domain.repo.ForecastRepositoryTest
import com.doapps.android.domain.viewModel.DashboardViewModelTest
import com.doapps.android.domain.viewModel.SearchViewModelTest
import com.doapps.android.domain.viewModel.WeatherDetailViewModelTest
import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.robolectric.annotation.Config



@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(Suite::class)
@Suite.SuiteClasses(
    DashboardViewModelTest::class,
    SearchViewModelTest::class,
    WeatherDetailViewModelTest::class,
    ForecastRepositoryTest::class,
    CurrentWeatherRepositoryTest::class
)
class TestSuite
