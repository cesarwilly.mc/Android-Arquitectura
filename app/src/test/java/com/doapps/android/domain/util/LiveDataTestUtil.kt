package com.doapps.android.domain.util

import com.doapps.android.model.general.*
import com.doapps.android.model.room.*


// Data Generators
fun createSampleForecastResponse(id: Int, cityName: String): ForecastEntity {
    val weatherItem =
        WeatherItem("12d", "clouds", "cloud & sun", 1)
    val weather = listOf(weatherItem)
    val listItem = ListItem(
        123123,
        Rain(12.0),
        "132121",
        Snow(12.0),
        weather,
        Main(34.0, 30.0, 2.0, 321.0, 21, 132.0, 12.0, 35.0),
        Clouds(1),
        Sys("a"),
        Wind(12.0, 12.0)
    )
    val list = listOf(listItem)
    return ForecastEntity(
        id,
        CityEntity(
            "Turkey",
            CoordEntity(34.0, 30.0),
            cityName,
            34
        ),
        list
    )
}



fun generateCurrentWeatherEntity(name: String, id: Int): CurrentWeatherEntity {
    val weatherItem =
        WeatherItem("12d", "clouds", "cloud & sun", 1)
    val weather = listOf(weatherItem)
    return CurrentWeatherEntity(
        1,
        2,
        MainEntity(34.0, 30.0, 2.0, 321.0, 21, 132.0, 12.0, 35.0),
        null,
        3421399123,
        weather,
        name,
        id,
        "Celciues",
        null
    )
}

fun createSampleForecastResponse(): ForecastResponse {
    val weatherItem =
        WeatherItem("12d", "clouds", "cloud & sun", 1)
    val weather = listOf(weatherItem)
    val listItem = ListItem(
        123123,
        Rain(12.0),
        "132121",
        Snow(12.0),
        weather,
        Main(34.0, 30.0, 2.0, 321.0, 21, 132.0, 12.0, 35.0),
        Clouds(1),
        Sys("a"),
        Wind(12.0, 12.0)
    )
    val list = listOf(listItem)
    return ForecastResponse(
        City(
            "Turkey",
            Coord(32.32, 30.30),
            "Istanbul",
            10
        ), null, null, null, list
    )
}

fun createSampleCurrentWeatherResponse(): CurrentWeatherResponse {
    val weatherItem =
        WeatherItem("12d", "clouds", "cloud & sun", 1)
    val weather = listOf(weatherItem)
    return CurrentWeatherResponse(
        null,
        null,
        Main(34.0, 30.0, 2.0, 321.0, 21, 132.0, 12.0, 35.0),
        Clouds(1),
        Sys("a"),
        null,
        Coord(32.32, 30.30),
        weather,
        "Istanbul",
        null,
        10,
        null,
        null
    )
}
