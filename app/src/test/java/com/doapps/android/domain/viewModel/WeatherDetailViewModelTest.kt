package com.doapps.android.domain.viewModel

import android.os.Build
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.doapps.android.domain.repo.forecast.ForecastRepositoryImpl
import com.doapps.android.model.room.ForecastEntity
import com.doapps.android.domain.ui.weather_detail.WeatherDetailViewModel
import com.doapps.android.domain.util.createSampleForecastResponse
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.robolectric.annotation.Config


@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(AndroidJUnit4::class)
class WeatherDetailViewModelTest: KoinTest{


    @MockK
    private lateinit var  weatherDetailViewModel: WeatherDetailViewModel
    @MockK
    private lateinit var forecastLocalDataSource: ForecastRepositoryImpl


    @Before
    fun before() {
        MockKAnnotations.init(this)
        weatherDetailViewModel=WeatherDetailViewModel(forecastLocalDataSource)
    }
    @After
    fun closeDatabase() {
        stopKoin()
    }

    @Test
    fun `insert forecast and when getForecast called the livedata result must be ForecastEntity`() {


        val observer : Observer<ForecastEntity> = mockk(relaxUnitFun = true)
        val slot = slot<ForecastEntity>()
        // When
        val currentWeatherLiveData: MutableLiveData<ForecastEntity> = MutableLiveData()
        currentWeatherLiveData.postValue(createSampleForecastResponse(1, "Istanbul"))
        every { weatherDetailViewModel.getForecast() } returns currentWeatherLiveData

        // Then
        weatherDetailViewModel.getForecast().observeForever(observer)
        every { observer.onChanged(capture(slot)) } answers {
            assertEquals("Istanbul", slot.captured.city?.cityName)
        }

    }
}
