package com.doapps.android.domain.core

import androidx.lifecycle.ViewModel


open class BaseViewModel : ViewModel()
