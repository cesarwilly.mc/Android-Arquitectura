package com.doapps.android.domain.ui.search


import androidx.lifecycle.*
import com.doapps.android.domain.repo.dataStore.DataStoreRepository
import com.doapps.android.domain.core.BaseViewModel
import com.doapps.android.domain.core.Constants
import com.doapps.android.model.room.CitiesForSearchEntity
import com.doapps.android.model.room.CoordEntity
import com.doapps.android.domain.usecase.SearchCitiesUseCase
import com.doapps.android.model.utils.status.Resource
import kotlinx.coroutines.*


class SearchViewModel (private val useCase: SearchCitiesUseCase, private val storeManager: DataStoreRepository) : BaseViewModel() {

    private val _searchParams: MutableLiveData<SearchCitiesUseCase.SearchCitiesParams> = MutableLiveData()
    fun getSearchViewState() = searchViewState

    private val searchViewState: LiveData<Resource<List<CitiesForSearchEntity>>> = _searchParams.switchMap { params ->
        useCase.execute(params)
    }

    fun setSearchParams(params: SearchCitiesUseCase.SearchCitiesParams) {
        if (_searchParams.value == params)
            return
        _searchParams.postValue(params)
    }

    fun saveCoordsToSharedPref(coordEntity: CoordEntity) = liveData<Boolean> {
        emit(false)
        GlobalScope.async {
            storeManager.setStringDataStore(Constants.Coords.LAT,coordEntity.lat.toString())
            storeManager.setStringDataStore(Constants.Coords.LON,coordEntity.lon.toString())
        }.await()
        emit(true)
    }
}
