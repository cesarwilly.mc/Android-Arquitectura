package com.doapps.android.domain.module


import com.doapps.android.domain.ui.dashboard.DashboardFragmentViewModel
import com.doapps.android.domain.ui.dashboard.forecast.ForecastItemViewModel
import com.doapps.android.domain.ui.main.MainActivityViewModel
import com.doapps.android.domain.ui.search.SearchViewModel
import com.doapps.android.domain.ui.search.result.SearchResultItemViewModel
import com.doapps.android.domain.ui.splash.SplashFragmentViewModel
import com.doapps.android.domain.ui.weather_detail.WeatherDetailViewModel
import com.doapps.android.domain.ui.weather_detail.weatherHourOfDay.WeatherHourOfDayItemViewModel

import org.koin.dsl.module


val viewModelModule = module {

    factory { WeatherDetailViewModel(get()) }
    factory { SplashFragmentViewModel(get()) }
    factory { SearchViewModel(get(),get()) }
    factory { DashboardFragmentViewModel(get(),get(),get()) }
    factory { MainActivityViewModel() }
    factory { WeatherHourOfDayItemViewModel() }
    factory { SearchResultItemViewModel() }
    factory { ForecastItemViewModel() }
}

