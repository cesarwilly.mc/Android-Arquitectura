package com.doapps.android.domain.ui.main

import androidx.databinding.ObservableField
import com.doapps.android.domain.core.BaseViewModel

class MainActivityViewModel : BaseViewModel() {
    var toolbarTitle: ObservableField<String> = ObservableField()
}
