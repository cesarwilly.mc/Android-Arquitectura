package com.doapps.android.domain.ui.dashboard

import android.transition.TransitionInflater
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.doapps.android.domain.R
import com.doapps.android.domain.core.BaseFragment
import com.doapps.android.domain.core.Constants
import com.doapps.android.domain.databinding.FragmentDashboardBinding
import com.doapps.android.domain.usecase.CurrentWeatherUseCase
import com.doapps.android.domain.usecase.ForecastUseCase
import com.doapps.android.domain.ui.dashboard.forecast.ForecastAdapter
import com.doapps.android.domain.ui.main.MainActivity
import com.doapps.android.domain.utils.extensions.isNetworkAvailable
import org.koin.androidx.viewmodel.ext.android.viewModel

class DashboardFragment :
    BaseFragment<DashboardFragmentViewModel, FragmentDashboardBinding>(R.layout.fragment_dashboard) {
    override val viewModel: DashboardFragmentViewModel by viewModel()
    override fun onCreateConfig() {
        super.onCreateConfig()
        initForecastAdapter()
        sharedElementReturnTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)

        runLiveDataStatusLatLng()


        binding.viewModel?.getForecastViewState()?.observeWith(
            viewLifecycleOwner
        ) {
            with(binding) {
                viewState = it

                it.data?.list?.let { forecasts -> initForecast(forecasts) }
                (requireActivity() as MainActivity).viewModel.toolbarTitle.set(it.data?.city?.getCityAndCountry())
            }
        }

        binding.viewModel?.getCurrentWeatherViewState()?.observeWith(
            viewLifecycleOwner
        ) {
            with(binding) {
                containerForecast.viewState = it
                containerForecast.data = it.data
            }
        }
    }

    private fun runLiveDataStatusLatLng() {
        viewModel.updateStatusLatandLong()
        if (viewModel.statusLatandLong.value == null) {
            viewModel.statusLatandLong.observe(this, {
                when (it.status) {
                    com.doapps.android.model.utils.status.Status.SUCCESS -> {
                        viewModel.setCurrentWeatherParams(
                            CurrentWeatherUseCase.CurrentWeatherParams(
                                it.data!![0],
                                it.data!![1],
                                isNetworkAvailable(requireContext()),
                                Constants.Coords.METRIC
                            )
                        )
                        viewModel.setForecastParams(
                            ForecastUseCase.ForecastParams(
                                it.data!![0],
                                it.data!![1],
                                isNetworkAvailable(requireContext()),
                                Constants.Coords.METRIC
                            )
                        )
                    }
                    else -> {}
                }
            })
        }
    }

    private fun initForecastAdapter() {
        val adapter = ForecastAdapter { item, cardView, forecastIcon, dayOfWeek, temp, tempMaxMin ->
            val action =
                DashboardFragmentDirections.actionDashboardFragmentToWeatherDetailFragment(item)
            findNavController()
                .navigate(
                    action,
                    FragmentNavigator.Extras.Builder()
                        .addSharedElements(
                            mapOf(
                                cardView to cardView.transitionName,
                                forecastIcon to forecastIcon.transitionName,
                                dayOfWeek to dayOfWeek.transitionName,
                                temp to temp.transitionName,
                                tempMaxMin to tempMaxMin.transitionName
                            )
                        )
                        .build()
                )
        }

        binding.recyclerForecast.adapter = adapter
        binding.recyclerForecast.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        postponeEnterTransition()
        binding.recyclerForecast.viewTreeObserver
            .addOnPreDrawListener {
                startPostponedEnterTransition()
                true
            }
    }

    private fun initForecast(list: List<com.doapps.android.model.general.ListItem>) {
        (binding.recyclerForecast.adapter as ForecastAdapter).submitList(list)
    }
}
