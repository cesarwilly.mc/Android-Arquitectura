package com.doapps.android.domain.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import com.doapps.android.domain.repo.dataStore.DataStoreRepository
import com.doapps.android.model.room.CurrentWeatherEntity
import com.doapps.android.model.room.ForecastEntity
import com.doapps.android.model.utils.exception.ExceptionGeneral
import com.doapps.android.domain.usecase.CurrentWeatherUseCase
import com.doapps.android.domain.usecase.ForecastUseCase
import com.doapps.android.model.utils.status.Resource
import com.doapps.android.domain.core.BaseViewModel
import com.doapps.android.domain.core.Constants
import com.doapps.android.domain.utils.Coroutines
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.withContext


class DashboardFragmentViewModel(
    private val forecastUseCase: ForecastUseCase,
    private val currentWeatherUseCase: CurrentWeatherUseCase,
    var storeManager: DataStoreRepository
) : BaseViewModel() {

    private val _forecastParams: MutableLiveData<ForecastUseCase.ForecastParams> = MutableLiveData()
    private val _currentWeatherParams: MutableLiveData<CurrentWeatherUseCase.CurrentWeatherParams> =
        MutableLiveData()

    fun getForecastViewState() = forecastViewState
    fun getCurrentWeatherViewState() = currentWeatherViewState

    private val forecastViewState: LiveData<Resource<ForecastEntity>> =
        _forecastParams.switchMap { params ->
            forecastUseCase.execute(params)
        }
    private val currentWeatherViewState: LiveData<Resource<CurrentWeatherEntity>> =
        _currentWeatherParams.switchMap { params ->
            currentWeatherUseCase.execute(params)
        }

    fun setForecastParams(params: ForecastUseCase.ForecastParams) {
        if (_forecastParams.value == params)
            return
        _forecastParams.postValue(params)
    }

    fun setCurrentWeatherParams(params: CurrentWeatherUseCase.CurrentWeatherParams) {
        if (_currentWeatherParams.value == params)
            return
        _currentWeatherParams.postValue(params)
    }

    val statusLatandLong =
        MutableLiveData<Resource<List<String>>>()

    fun updateStatusLatandLong() {
        Coroutines.io {
            try {
                withContext(Dispatchers.IO) {
                    statusLatandLong.postValue(
                        Resource.loading(
                            null
                        )
                    )
                    val lat =
                        async { storeManager.getStringDataStoreFlow(Constants.Coords.LAT).first() }
                    val lon =
                        async { storeManager.getStringDataStoreFlow(Constants.Coords.LON).first() }
                    if (lat.await().isNotEmpty() && lon.await().isNotEmpty()) {
                        statusLatandLong.postValue(
                            Resource.success(
                                listOf(lat.await(), lon.await())
                            )
                        )
                    } else {
                        statusLatandLong.postValue(
                            Resource.success(
                                null
                            )
                        )
                    }
                }
            } catch (e: ExceptionGeneral) {
                statusLatandLong.postValue(
                    Resource.error(
                        e,
                        null
                    )
                )
            }
        }
    }
}
