package com.doapps.android.domain.ui.weather_detail.weatherHourOfDay

import androidx.databinding.ObservableField
import com.doapps.android.model.general.ListItem
import com.doapps.android.domain.core.BaseViewModel



class WeatherHourOfDayItemViewModel  : BaseViewModel() {
    var item = ObservableField<ListItem>()
}
