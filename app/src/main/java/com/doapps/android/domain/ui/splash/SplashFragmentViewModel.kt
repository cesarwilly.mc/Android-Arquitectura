package com.doapps.android.domain.ui.splash


import androidx.lifecycle.MutableLiveData
import com.doapps.android.domain.repo.dataStore.DataStoreRepository
import com.doapps.android.domain.core.BaseViewModel
import com.doapps.android.domain.core.Constants
import com.doapps.android.domain.utils.Coroutines


class SplashFragmentViewModel(var storeManager: DataStoreRepository) : BaseViewModel() {
    var visibilityButtonExplore = MutableLiveData<Boolean>()
    var navigateDashboard = MutableLiveData<Boolean>()

    init {
        settingsDashboardFirstTimeOrNot()
    }

    private fun settingsDashboardFirstTimeOrNot() {
        Coroutines.io {
            if (storeManager.getStringDataStore(Constants.Coords.LON).isNullOrEmpty()) {
                navigateDashboard.postValue(false)
                visibilityButtonExplore.postValue(true)
            } else {
                navigateDashboard.postValue(true)
                visibilityButtonExplore.postValue(false)
            }
        }
    }
}
