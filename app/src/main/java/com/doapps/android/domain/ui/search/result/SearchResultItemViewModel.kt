package com.doapps.android.domain.ui.search.result

import androidx.databinding.ObservableField
import com.doapps.android.domain.core.BaseViewModel
import com.doapps.android.model.room.CitiesForSearchEntity




class SearchResultItemViewModel : BaseViewModel() {
    var item = ObservableField<CitiesForSearchEntity>()
}
