package com.doapps.android.domain.ui.weather_detail

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.doapps.android.model.general.ListItem
import com.doapps.android.domain.core.BaseViewModel
import com.doapps.android.model.room.ForecastEntity
import com.doapps.android.domain.repo.forecast.ForecastRepository

class WeatherDetailViewModel (private val forecastLocalDataSource: ForecastRepository) : BaseViewModel() {

    var weatherItem: ObservableField<ListItem> = ObservableField()
    private var forecastLiveData: LiveData<ForecastEntity> = MutableLiveData()
    var selectedDayDate: String? = null
    var selectedDayForecastLiveData: MutableLiveData<List<ListItem>> = MutableLiveData()

    fun getForecastLiveData() = forecastLiveData

    fun getForecast(): LiveData<ForecastEntity> {
        return forecastLocalDataSource.getForecast()
    }
}
