package com.doapps.android.domain.ui.dashboard.forecast

import androidx.databinding.ObservableField
import com.doapps.android.model.general.ListItem
import com.doapps.android.domain.core.BaseViewModel



class ForecastItemViewModel  : BaseViewModel() {
    var item = ObservableField<ListItem>()
}
