package com.doapps.android.model.utils.status

import com.doapps.android.model.utils.exception.ExceptionGeneral


open class BaseViewState(private val baseStatus: Status, private val baseError: ExceptionGeneral?) {
    fun isLoading() = baseStatus == Status.LOADING
    fun getErrorMessage() = baseError
    fun shouldShowErrorMessage() = baseError != null
}