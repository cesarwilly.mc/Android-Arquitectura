package com.doapps.android.model.utils.status




enum class Status {
    SUCCESS,
    LOADING,
    ERROR
}
