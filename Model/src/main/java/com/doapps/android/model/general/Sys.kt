package com.doapps.android.model.general

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Sys(

    @SerializedName( "pod")
    val pod: String?
) : Parcelable
