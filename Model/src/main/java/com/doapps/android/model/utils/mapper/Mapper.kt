package com.doapps.android.model.utils.mapper


interface Mapper<R, D> {
    fun mapFrom(type: R): D
}
