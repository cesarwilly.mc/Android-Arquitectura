package com.doapps.android.conexionmodule

import com.doapps.android.model.general.*
import com.doapps.android.model.room.*


// Data Generators
fun createSampleForecastResponse(id: Int, cityName: String): ForecastEntity {
    val weatherItem =
        WeatherItem("12d", "clouds", "cloud & sun", 1)
    val weather = listOf(weatherItem)
    val listItem = ListItem(
        123123,
        Rain(12.0),
        "132121",
        Snow(12.0),
        weather,
        Main(34.0, 30.0, 2.0, 321.0, 21, 132.0, 12.0, 35.0),
        Clouds(1),
        Sys("a"),
        Wind(12.0, 12.0)
    )
    val list = listOf(listItem)
    return ForecastEntity(
        id,
        CityEntity(
            "Turkey",
            CoordEntity(34.0, 30.0),
            cityName,
            34
        ),
        list
    )
}

fun createSampleForecastWithCoord(id: Int, cityName: String, lat: Double, lon: Double): ForecastEntity {
    val list = emptyList<ListItem>()
    return ForecastEntity(
        id,
        CityEntity(
            "Turkey",
            CoordEntity(lon, lat),
            cityName,
            34
        ),
        list
    )
}

fun generateCitiesForSearchEntity(id: String, name: String): CitiesForSearchEntity {
    return CitiesForSearchEntity(
        "Clear",
        "Turkey",
        CoordEntity(34.0, 30.0),
        name,
        "Beyoglu",
        1,
        id
    )
}

fun generateCurrentWeatherEntity(name: String, id: Int): CurrentWeatherEntity {
    val weatherItem =
        WeatherItem("12d", "clouds", "cloud & sun", 1)
    val weather = listOf(weatherItem)
    return CurrentWeatherEntity(
        1,
        2,
        MainEntity(34.0, 30.0, 2.0, 321.0, 21, 132.0, 12.0, 35.0),
        null,
        3421399123,
        weather,
        name,
        id,
        "Celciues",
        null
    )
}

