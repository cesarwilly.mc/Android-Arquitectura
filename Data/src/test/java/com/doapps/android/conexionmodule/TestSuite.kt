package com.doapps.android.conexionmodule

import android.os.Build
import com.doapps.android.conexionmodule.dao.CitiesForSearchDaoTest
import com.doapps.android.conexionmodule.dao.CurrentWeatherDaoTest
import com.doapps.android.conexionmodule.dao.ForecastDaoTest
import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.robolectric.annotation.Config



@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(Suite::class)
@Suite.SuiteClasses(
    CitiesForSearchDaoTest::class,
    CurrentWeatherDaoTest::class,
    ForecastDaoTest::class,
)
class TestSuite
