package com.doapps.android.conexionmodule.db.dataStore

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import androidx.datastore.preferences.preferencesDataStore


class DataStoreManagerImpl(private val context: Context) : DataStoreManager {

    val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "weatherAppDataStore")

    override  fun getDataStore() = context.dataStore
}