package com.doapps.android.conexionmodule.module

import com.doapps.android.conexionmodule.db.dataStore.DataStoreManager
import com.doapps.android.conexionmodule.db.dataStore.DataStoreManagerImpl
import com.doapps.android.conexionmodule.db.room.WeatherDatabase
import org.koin.dsl.module


val databaseModule = module {
    factory<DataStoreManager> {  DataStoreManagerImpl(get()) }
    single<WeatherDatabase> {  WeatherDatabase(get()) }
    factory {  get<WeatherDatabase>().forecastDao() }
    factory {  get<WeatherDatabase>().currentWeatherDao() }
    factory {  get<WeatherDatabase>().citiesForSearchDao() }
}
