package com.doapps.android.conexionmodule.datasource.forecast

import com.doapps.android.conexionmodule.network.WeatherAppAPI
import com.doapps.android.model.general.ForecastResponse
import retrofit2.Response


interface ForecastRemoteDataSource {

    suspend fun getForecastByGeoCords(
        lat: Double,
        lon: Double,
        units: String
    ): Response<ForecastResponse>
}

class ForecastRemoteDataSourceImpl(private val api: WeatherAppAPI) : ForecastRemoteDataSource {

    override suspend fun getForecastByGeoCords(
        lat: Double,
        lon: Double,
        units: String
    ): Response<ForecastResponse> = api.getForecastByGeoCords(lat, lon, units)
}
