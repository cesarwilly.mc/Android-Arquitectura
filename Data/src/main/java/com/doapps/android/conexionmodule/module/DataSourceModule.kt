package com.doapps.android.conexionmodule.module

import com.doapps.android.conexionmodule.datasource.currentWeather.CurrentWeatherLocalDataSource
import com.doapps.android.conexionmodule.datasource.currentWeather.CurrentWeatherLocalDataSourceImpl
import com.doapps.android.conexionmodule.datasource.currentWeather.CurrentWeatherRemoteDataSource
import com.doapps.android.conexionmodule.datasource.currentWeather.CurrentWeatherRemoteDataSourceImpl
import com.doapps.android.conexionmodule.datasource.dataStore.DataStoreDataSource
import com.doapps.android.conexionmodule.datasource.dataStore.DataStoreDataSourceImpl
import com.doapps.android.conexionmodule.datasource.forecast.ForecastLocalDataSource
import com.doapps.android.conexionmodule.datasource.forecast.ForecastLocalDataSourceImpl
import com.doapps.android.conexionmodule.datasource.forecast.ForecastRemoteDataSource
import com.doapps.android.conexionmodule.datasource.forecast.ForecastRemoteDataSourceImpl
import com.doapps.android.conexionmodule.datasource.searchCities.SearchCitiesLocalDataSource
import com.doapps.android.conexionmodule.datasource.searchCities.SearchCitiesLocalDataSourceImpl
import com.doapps.android.conexionmodule.datasource.searchCities.SearchCitiesRemoteDataSource
import com.doapps.android.conexionmodule.datasource.searchCities.SearchCitiesRemoteDataSourceImpl
import org.koin.dsl.module

val dataSourceModule = module {
    factory <CurrentWeatherLocalDataSource>{ CurrentWeatherLocalDataSourceImpl(get()) }
    factory <CurrentWeatherRemoteDataSource>{ CurrentWeatherRemoteDataSourceImpl(get()) }

    factory <ForecastLocalDataSource> { ForecastLocalDataSourceImpl(get()) }
    factory <ForecastRemoteDataSource> { ForecastRemoteDataSourceImpl(get()) }

    factory <SearchCitiesLocalDataSource>{ SearchCitiesLocalDataSourceImpl(get()) }
    factory <SearchCitiesRemoteDataSource>{ SearchCitiesRemoteDataSourceImpl(get()) }
    factory <DataStoreDataSource> { DataStoreDataSourceImpl(get()) }
}