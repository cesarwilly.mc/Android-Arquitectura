package com.doapps.android.conexionmodule.datasource.searchCities

import androidx.lifecycle.LiveData
import com.doapps.android.conexionmodule.db.room.dao.CitiesForSearchDao
import com.doapps.android.model.general.SearchResponse
import com.doapps.android.model.room.CitiesForSearchEntity

interface SearchCitiesLocalDataSource {

    fun getCityByName(cityName: String?): LiveData<List<CitiesForSearchEntity>>

    fun insertCities(response: SearchResponse)
}

class SearchCitiesLocalDataSourceImpl(private val citiesForSearchDao: CitiesForSearchDao) :
    SearchCitiesLocalDataSource {

    override fun getCityByName(cityName: String?): LiveData<List<CitiesForSearchEntity>> =
        citiesForSearchDao.getCityByName(cityName)

    override fun insertCities(response: SearchResponse) {
        response.hits
            ?.map { CitiesForSearchEntity(it) }
            ?.let { citiesForSearchDao.insertCities(it) }
    }
}
