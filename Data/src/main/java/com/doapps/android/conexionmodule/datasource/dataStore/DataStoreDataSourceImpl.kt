package com.doapps.android.conexionmodule.datasource.dataStore

import androidx.datastore.preferences.core.*
import com.doapps.android.conexionmodule.db.dataStore.DataStoreManager
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

class DataStoreDataSourceImpl(dataStoreManager: DataStoreManager):DataStoreDataSource {

    private val dataStore=dataStoreManager.getDataStore()

    override  fun getIntDataStoreFlow(key: String): Flow<Int> {
        val preferenceKey = intPreferencesKey(key)
        return  dataStore.data
            .map { preferences ->
                preferences[preferenceKey] ?: 0
            }
    }

    override suspend fun getIntDataStore(key: String): Int {
        val preferenceKey = intPreferencesKey(key)
        return  dataStore.data
            .map { preferences ->
                preferences[preferenceKey] ?: 0
            }.first()
    }

    override fun getStringDataStoreFlow(key:String): Flow<String> {
        val preferenceKey = stringPreferencesKey(key)
        return  dataStore.data
            .map { preferences ->
                preferences[preferenceKey] ?: ""
            }
    }

    override suspend fun getStringDataStore(key: String): String {
        val preferenceKey = stringPreferencesKey(key)
        return  dataStore.data
            .map { preferences ->
                preferences[preferenceKey] ?: ""
            }.first()
    }

    override fun getBooleanDataStore(key:String): Flow<Boolean> {
        val preferenceKey = booleanPreferencesKey(key)
        return  dataStore.data
            .map { preferences ->
                preferences[preferenceKey] ?: false
            }
    }
    override fun getFloatDataStore(key:String): Flow<Float> {
        val preferenceKey = floatPreferencesKey(key)
        return  dataStore.data
            .map { preferences ->
                preferences[preferenceKey] ?: 0f
            }
    }

    //Setter
    override suspend fun setIntDataStore(key:String,value:Int) {
        val preferenceKey = intPreferencesKey(key)
        dataStore.edit { setting->
            setting[preferenceKey] = value
        }.toMutablePreferences()

    }
    override suspend fun setStringDataStore(key:String, value:String){
        val preferenceKey = stringPreferencesKey(key)
        dataStore.edit { setting->
            setting[preferenceKey] = value
        }
    }
    override suspend fun setBooleanDataStore(key:String, value:Boolean){
        val preferenceKey = booleanPreferencesKey(key)
        dataStore.edit { setting->
            setting[preferenceKey] = value
        }
    }
    override suspend fun setFloatDataStore(key:String, value:Float){
        val preferenceKey = floatPreferencesKey(key)
        dataStore.edit { setting->
            setting[preferenceKey] = value
        }
    }
    override suspend  fun deleteAllData(){
        dataStore.edit { it.clear() }
    }

}