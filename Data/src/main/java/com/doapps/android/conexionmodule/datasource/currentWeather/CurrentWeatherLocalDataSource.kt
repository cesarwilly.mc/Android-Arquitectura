package com.doapps.android.conexionmodule.datasource.currentWeather

import androidx.lifecycle.LiveData
import com.doapps.android.conexionmodule.db.room.dao.CurrentWeatherDao
import com.doapps.android.model.general.CurrentWeatherResponse
import com.doapps.android.model.room.CurrentWeatherEntity



interface CurrentWeatherLocalDataSource  {

    fun getCurrentWeather(): LiveData<CurrentWeatherEntity>

    suspend fun insertCurrentWeather(currentWeather: CurrentWeatherResponse)
}


class CurrentWeatherLocalDataSourceImpl (private val currentWeatherDao: CurrentWeatherDao):CurrentWeatherLocalDataSource {

    override fun getCurrentWeather() = currentWeatherDao.getCurrentWeather()

    override suspend fun insertCurrentWeather(currentWeather: CurrentWeatherResponse) = currentWeatherDao.deleteAndInsert(
        CurrentWeatherEntity(currentWeather)
    )
}
