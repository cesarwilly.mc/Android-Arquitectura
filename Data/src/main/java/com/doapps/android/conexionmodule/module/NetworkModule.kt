package com.doapps.android.conexionmodule.module

import com.algolia.search.saas.places.PlacesClient
import com.doapps.android.conexionmodule.network.WeatherAppAPI
import com.doapps.android.conexionmodule.network.config.DefaultRequestInterceptor
import com.doapps.android.conexionmodule.network.config.HttpConfiguration
import com.doapps.android.model.utils.Constants
import org.koin.dsl.module


val networkModule = module {

    val places: PlacesClient =
        PlacesClient(Constants.AlgoliaKeys.APPLICATION_ID, Constants.AlgoliaKeys.SEARCH_API_KEY)
    factory<PlacesClient> { places }
    factory {  DefaultRequestInterceptor(get()) }
    factory {  HttpConfiguration(get()) }
    factory<WeatherAppAPI> {
        WeatherAppAPI.invoke(get())
    }
}