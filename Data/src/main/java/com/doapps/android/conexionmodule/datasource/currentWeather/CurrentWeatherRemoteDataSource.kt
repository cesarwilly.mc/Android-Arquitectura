package com.doapps.android.conexionmodule.datasource.currentWeather

import com.doapps.android.conexionmodule.network.WeatherAppAPI
import com.doapps.android.model.general.CurrentWeatherResponse
import retrofit2.Response


interface CurrentWeatherRemoteDataSource {
    suspend fun getCurrentWeatherByGeoCords(
        lat: Double,
        lon: Double,
        units: String
    ): Response<CurrentWeatherResponse>
}


class CurrentWeatherRemoteDataSourceImpl(private val api: WeatherAppAPI) :
    CurrentWeatherRemoteDataSource {
    override suspend fun getCurrentWeatherByGeoCords(
        lat: Double,
        lon: Double,
        units: String
    ): Response<CurrentWeatherResponse> = api.getCurrentByGeoCords(lat, lon, units)
}
