package com.doapps.android.conexionmodule.datasource.dataStore

import kotlinx.coroutines.flow.Flow



interface DataStoreDataSource {

    fun getIntDataStoreFlow(key: String): Flow<Int>

    suspend fun getIntDataStore(key: String): Int

    fun getStringDataStoreFlow(key: String): Flow<String>

    suspend fun getStringDataStore(key: String): String

    fun getBooleanDataStore(key: String): Flow<Boolean>

    fun getFloatDataStore(key: String): Flow<Float>

    suspend fun setIntDataStore(key: String, value: Int)

    suspend fun setStringDataStore(key: String, value: String)

    suspend fun setBooleanDataStore(key: String, value: Boolean)

    suspend fun setFloatDataStore(key: String, value: Float)

    suspend fun deleteAllData()

}