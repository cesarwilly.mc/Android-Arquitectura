package com.doapps.android.conexionmodule.db.dataStore

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences


interface DataStoreManager {
    fun getDataStore(): DataStore<Preferences>
}