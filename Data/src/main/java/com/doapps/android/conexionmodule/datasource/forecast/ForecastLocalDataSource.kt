package com.doapps.android.conexionmodule.datasource.forecast

import androidx.lifecycle.LiveData
import com.doapps.android.conexionmodule.db.room.dao.ForecastDao
import com.doapps.android.model.general.ForecastResponse
import com.doapps.android.model.room.ForecastEntity


interface ForecastLocalDataSource{

    fun getForecast(): LiveData<ForecastEntity>
    fun insertForecast(forecast: ForecastResponse)
}

class ForecastLocalDataSourceImpl (private val forecastDao: ForecastDao):ForecastLocalDataSource {

    override fun getForecast() = forecastDao.getForecast()

    override fun insertForecast(forecast: ForecastResponse) = forecastDao.deleteAndInsert(
        ForecastEntity(forecast)
    )
}
