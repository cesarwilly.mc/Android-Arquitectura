# Clean Architecture Project

Mobile application based on https://github.com/furkanaskin/Weatherapp and modified by @cesarwillymc

# Table of Contents

1. [Architecture](#architecture)
1. [Data](#data)
1. [Model](#model)
1. [Domain](#domain)
1. [App](#app)
1. [Unit Test](#unit-test)
1. [Settings](#settings)

# Architecture

## Usa los conceptos SOLID

* Single Responsability: Cada clase o funcion solo cumple un rol.

* Open/Close: Se usa en el gestor de estados Resource.

* Liskov Substitution: Las clases que se heredan deben guardar coherencia entre padre e hijo.

* Interface Segregation: No se debe implementar interfaces o funciones que nunca se usan

* Dependency Injection: El módulo de alto nivel no debe depender del módulo de bajo nivel, conexion
  nunca dependera de app, y app tampoco tendra comunicacion o algun metodo directo al modulo
  conexion.

![1](assets/clean.png)

**[⬆ back to top](#table-of-contents)**

# Data

En este modulo, se usara especificamente la para conexion tanto localmente, como en red. Usando
tecnologias como Voley, Retrofit, Http, Room, SQLite, Shared Preferences, DataStore, y mas. Este
modulo solo podra ser implementado en Domain, Ningun modulo mas lo puede acceder, para mantener la
arquitectura.

## Main

### DataBase

![9](assets/db.JPG)

Aqui se debe separar entre los gestores de base de datos locales, en este caso si se usara un nuevo
gestor como Shared se recomienda adjuntarlo en otra carpeta.

* Data Source es un Shared preference mejorado que trabaja con Flow y coroutines

* Como comenta la sigla `D` de `SOLID` el acceso a las dependencias no deben de ser directas sino
  mediante interfaces. Es por esto que para acceder a `DataStoreManagerImpl` se ingresara mediante
  la interfaz previamente lo cual asegura que no se tenga comunicacion directa con el core, eso pasa
  igualmente en Room. Con esta prohibicion hacemos que cualquier modulo al implementar esta clase no
  pueda hacer modificaciones completas, unicamente acceso a la interfaz y sus funciones.

* Agregar unicamente funciones y clases necesarias para esta clase y que solo son necesitadas en
  esta, por ejemplo un `TypeConverts` solamente sera accedido por este modulo, ya que solamente se
  necesita en este modulo.

### Module

Aqui unicamente se agregaran los modulos para la injeccion de dependencias con KOIN.

```kt
val databaseModule = module {
    single<DataStoreManager> {  DataStoreManagerImpl(get()) }
    single<WeatherDatabase> {  WeatherDatabase(get()) }
    factory {  get<WeatherDatabase>().forecastDao() }
    factory {  get<WeatherDatabase>().currentWeatherDao() }
    factory {  get<WeatherDatabase>().citiesForSearchDao() }
}
val networkModule = module {
    val places: PlacesClient =
        PlacesClient(Constants.AlgoliaKeys.APPLICATION_ID, Constants.AlgoliaKeys.SEARCH_API_KEY)
    factory<PlacesClient> { places }
    factory {  DefaultRequestInterceptor(get()) }
    factory {  HttpConfiguration(get()) }
    factory<WeatherAppAPI> {
        WeatherAppAPI.invoke(get())
    }
}
```
### DataSource

Sera la capa de conexion directa con el `network` y la `db`.

![3](assets/dataSource.JPG)

### Network

![2](assets/network.JPG)

Se recomienda usar la configuracion siguiente.

#### DefaultRequestInterceptor

En caso se requiera enviar un query para cada consulta o un header con el token puede usarla de esta
forma.

```kt
class DefaultRequestInterceptor(private val dataStore: DataStoreManager) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val url = chain.request().url
            .newBuilder()
            .addQueryParameter(BuildConfig.API_KEY_QUERY, BuildConfig.API_KEY_VALUE)
            .build()
        val request =
            chain.request().newBuilder().url(url).addHeader("Authorization", getTokenData()).build()
        return chain.proceed(request)

    }

    private fun getTokenData(): String {
        var token = ""
        GlobalScope.launch(Dispatchers.Main) {
            token = dataStore.getStringDataStore("token").first()
        }
        return token
    }
}
```

#### HttpConfiguration

En esta clase retornaremos el http builder.

```kt
OkHttpClient.Builder()
            .readTimeout(timeOut.toLong(), TimeUnit.SECONDS)
            .writeTimeout(timeOut.toLong(), TimeUnit.SECONDS)
            .connectTimeout(timeOut.toLong(), TimeUnit.SECONDS)
            .addInterceptor(defaultRequestInterceptor)
            .cache(cache)
            .build()
```

#### API

En esta interfaz asignaremos la conexion con la api, e invocaremos al metodo de conexion.

```kt
companion object {
        operator fun invoke(httpConfiguration: HttpConfiguration): WeatherAppAPI {
            val okHttpClient= httpConfiguration.onCreate()
            return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WeatherAppAPI::class.java)
        }
    }
```



**[⬆ back to top](#table-of-contents)**

# Model

En este modulo asignaremos las clases de uso compartido tanto para el `Domain`, `Conexion`, `App`
como las entidades de la base de datos, los modelos genericos y utils logic como
el `gestor de estados`, algunas `extensions` de logica, `mappers`, `exceptions`, `Constants`.

## Main

### General Models

Esta carpeta sera dividida dependiendo de las responsabilidades y no agrupadas en una sola carpeta
todos los modelos.

* Un modelo por defecto debe ser un DataClass.

```kt
@Parcelize
data class City(
    @SerializedName( "country")
    val country: String?,

    @SerializedName( "coord")
    val coord: Coord?,

    @SerializedName( "name")
    val name: String?,

    @SerializedName( "id")
    val id: Int?
) : Parcelable
```

### Models Room

Son modelos usadas por Room tienen configuraciones diferentes a un modelo generico.

```kt
@Parcelize
@Entity(tableName = "City")
data class CityEntity(
    @ColumnInfo(name = "cityCountry")
    var cityCountry: String?,
    @Embedded
    var cityCoord: CoordEntity?,
    @ColumnInfo(name = "cityName")
    var cityName: String?,
    @ColumnInfo(name = "cityId")
    var cityId: Int?
) : Parcelable 
```

### Utils Logic

Aqui tendremos toda la logica de negocio compartida, ya sean pequeñas comprobaciones
genericas, `gestor de estados`, algunas `extensions` de logica, `mappers`, `exceptions`, `Constants`
.

* Por ejemplo vemos un exception generico que se usara para errores tanto en el domain como en la
  UI.

* Identificar correctamente cuales son los utils genericos, para no tener problemas luego.

```kt
class ExceptionGeneral(messageD: String, codigo: Int = 0) :
    Exception(if (messageD.contains("handshake") || 
        messageD.contains("Unable to resolve host")) 
            "Revisa tu conexión a internet" else messageD) {
    var code: Int = 0

    init {
        code = codigo
    }
}
```

**[⬆ back to top](#table-of-contents)**

# Domain

En este modulo se realiza todo el procesamiento de datos, es el nucleo de nuestro aplicativo, Esta
capa no deberia depender de ninguna otra dependencia o capa.

## Main

### Module

Aqui unicamente se agregaran los modulos para la injeccion de dependencias con KOIN.

Obtendremos el modulo del Network y lo añadiremos al modulo actual para que luego sea leido en el
aplication.

```kt
val listOfModules = listOf(databaseModule, networkModule, repositoryModule, useCaseModule,
    dataSourceModule)
```


### Repository

* Es la unica capa que realiza conexion con el DataSource en el Modulo de `Data` o `Conexion`.

* En esta carpeta se implementara la logica o tratamiento de datos, ya sea el uso de NetworkBound
  que eligue si busca la informacion en la web o en la base de datos dependiendo de los parametros
  como tiempo, obligacion u otros.

* Tambien podemos implementar otra logica no necesariamente el NetworkBound, trabajarlo con flow, o
  pasarlo directamente a los casos de Uso.

![4](assets/repo.JPG)

```kt
class CurrentWeatherRepositoryImpl(
    private val currentWeatherRemoteDataSource: CurrentWeatherRemoteDataSource,
    private val currentWeatherLocalDataSource: CurrentWeatherLocalDataSource
) : SafeApiRequest(), CurrentWeatherRepository {

    override val currentWeatherRateLimit =
        RateLimiter<String>(30, TimeUnit.SECONDS)

    override fun loadCurrentWeatherByGeoCords(
        lat: Double,
        lon: Double,
        fetchRequired: Boolean,
        units: String
    ): LiveData<Resource<CurrentWeatherEntity>> {
        return object : NetworkBoundResource<CurrentWeatherEntity, CurrentWeatherResponse>() {
            override suspend fun saveCallResult(item: CurrentWeatherResponse) =
                currentWeatherLocalDataSource.insertCurrentWeather(item)

            override fun shouldFetch(data: CurrentWeatherEntity?): Boolean = fetchRequired

            override fun loadFromDb(): LiveData<CurrentWeatherEntity> =
                currentWeatherLocalDataSource.getCurrentWeather()

            override suspend fun createCall(): CurrentWeatherResponse = apiRequest {
                currentWeatherRemoteDataSource.getCurrentWeatherByGeoCords(
                    lat,
                    lon,
                    units
                )
            }

            override fun onFetchFailed() = currentWeatherRateLimit.reset(RATE_LIMITER_TYPE)
        }.asLiveData
    }
}
```

### Use case

* En los casos de uso crearemos los parametros que se necesitan para el envio de datos y obtendremos
  la data ya procesada.

```kt
class CurrentWeatherUseCase(private val repository: CurrentWeatherRepository) :
    UseCaseLiveData<Resource<CurrentWeatherEntity>, CurrentWeatherUseCase.CurrentWeatherParams, CurrentWeatherRepositoryImpl>() {

    override fun buildUseCaseObservable(params: CurrentWeatherParams?): LiveData<Resource<CurrentWeatherEntity>> {
        return repository.loadCurrentWeatherByGeoCords(
            params?.lat?.toDouble() ?: 0.0,
            params?.lon?.toDouble() ?: 0.0,
            params?.fetchRequired
                ?: false,
            units = params?.units ?: Constants.Coords.METRIC
        ).map {
            it
        }
    }

    class CurrentWeatherParams(
        val lat: String = "",
        val lon: String = "",
        val fetchRequired: Boolean,
        val units: String
    ) : Params()
}
```

**[⬆ back to top](#table-of-contents)**

# App

## Main

### Core

![5](assets/core.JPG)

En esta carpeta nosotros implementaremos todas las base como vemos algunas extensions para la parte
del xml, constantes, base activity para gestionar los View Model y los DataBinding, y otras cosas
mas.

### UI

![6](assets/ui.JPG)

En este paquete se Crearan los viewModel y haran conexion con los fragment.

```kt
class SplashFragmentViewModel(var storeManager: DataStoreSource) : BaseViewModel() {
    var visibilityButtonExplore = MutableLiveData<Boolean>()
    var navigateDashboard = MutableLiveData<Boolean>()

    init {
        settingsDashboardFirstTimeOrNot()
    }

    private fun settingsDashboardFirstTimeOrNot() {
        Coroutines.io {
            if (storeManager.getStringDataStore(Constants.Coords.LON).isNullOrEmpty()) {
                navigateDashboard.postValue(false)
                visibilityButtonExplore.postValue(true)
            } else {
                navigateDashboard.postValue(true)
                visibilityButtonExplore.postValue(false)
            }
        }
    }
}
```

en el fragment por ejemplo usando el base fragment se reduce el codigo de esa manera.

```kt
class SplashFragment :
    BaseFragment<SplashFragmentViewModel, FragmentSplashBinding>(R.layout.fragment_splash) {
    override val viewModel: SplashFragmentViewModel by viewModel()

    override fun onCreateConfig() {
        super.onCreateConfig()
        viewModel.navigateDashboard.observe(this, { navigate ->
            if (navigate != null) {
                startSplashAnimation(navigate)
                binding.buttonExplore.setOnClickListener {
                    endSplashAnimation(navigate)
                }
                binding.rootView.setOnClickListener {
                    endSplashAnimation(navigate)
                }
            }
        })
    }
```

### Utils UI

![7](assets/utils.JPG)

En este paquete usamos extension para la UI como:

```kt
fun View.hide() {
    visibility = GONE
}

fun View.show() {
    visibility = VISIBLE
}
```

# Unit Test

Las pruebas unitarias estaran en cada modulo.

Aqui realizaremos las pruebas de unit test tambien, serparando por clases y packetes para las
pruebas unitarias se esta usando Mockk y JUnit con RoboElectric.

* Es importante parar el kodein antes de cada prueba e inicializarlo, aqui realizaremos las pruebas
  para el Domain como de App. Podemos mockear los valos o injectar los modulos.

```kt
    @MockK
    lateinit var currentWeatherRemoteDataSource: CurrentWeatherRemoteDataSource

    @MockK
    lateinit var currentWeatherLocalDataSource: CurrentWeatherLocalDataSource
```

* Es importante `detener`  el koin antes de cada prueba e inicializarlo nuevamente con los modulos
  necesitados en caso de hacer pruebas con la base de datos.

```kt
    @Before
    fun before() {
        stopKoin() // to remove 'A Koin Application has already been started'
        startKoin {
            androidContext(ApplicationProvider.getApplicationContext())
            modules(dbTestModule)
        }
    }
```

**[⬆ back to top](#table-of-contents)**

# Settings

## APP module

En nuestro app generaremos esto, para obtener u contexto de forma global.

```kt
companion object{
        private lateinit var instance:WeatherApp
        fun getContextApp(): Context =instance
        fun setInstance(instance:WeatherApp){
            this.instance=instance
        }
    }
```

En el oncreate detenemos el koin, instanciamos el contexto, inicializamos Bugsnag para detectar
errores, e iniciamos kodein con los modulos propios como de domain.

```kt
    override fun onCreate() {
        super.onCreate()
        //Stop koin for if its open
        stopKoin()
        setInstance(this)
        //Init bugsnag for check errors
        Bugsnag.start(this)
        //Init koin dependency injection
        startKoin {
            // Koin Android logger
            androidLogger()

            //inject Android context
            androidContext(this@WeatherApp)
            // use modules
            modules(module(override = true) {
               modules(listOfModules + viewModelModule )
            })
        }
    }
```

Y luego para que este sea usado correctamente agregar en el name nuestro APP.

```sh
<application
        ${Implementacion del APP: android:name=".application.App"}
        android:allowBackup="true"
        android:icon="@mipmap/ic_launcher"
        android:label="@string/app_name"
        android:roundIcon="@mipmap/ic_launcher_round"
        android:supportsRtl="true"
        android:theme="@style/AppTheme"
        tools:ignore="AllowBackup">

    </application>
```

## Activity

Al crear un activity se tiene que respetar la siguiente nomenclatura para la clase `NombreActivity`
cada clase que sea activity tiene que terminar con la palabra activity recuerda que el nombre tiene
que ser corto y especifico a la función que se le dará al activity por ejemplo:

- UserActivity
- UserListActivity
- UserUpdateActivity

Recuerda crear un paquete llamado activity que encierre todo los activitys en el caso
del `splashActivity` es opcional debido que es la presentación de la app y no lleva mayor lógica que
verificar si en la app un usuario sea loguiado.

### Base Activity

* Para usar el Base Activity es un requerimiento pasar un viewModel, y un DataBinding, y medinte un
  parametro el layout.

* Como veremos iniciaremos el DataBinding de dos formas con un tryCatch.

* Te pedira implementar el `onCreateLogic` en el Hijo, ahi ya podras usar todas las funciones que
  creas conveniente.

```kt
abstract class BaseActivity<ViewModel : BaseViewModel, DB : ViewDataBinding>(@LayoutRes val layout: Int) :
    AppCompatActivity() {
    abstract val viewModel: ViewModel

    open lateinit var binding: DB
    
    private fun initBinding() {
        try {
            binding = DataBindingUtil.setContentView(this, layout)

        } catch (e: Exception) {
            binding = DataBindingUtil.inflate(layoutInflater, layout, null, false)
            setContentView(binding.root)
        }
        binding.lifecycleOwner = this
    }

    open fun onCreateLogic() {}
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBinding()
        onCreateLogic()
    }

}
```

## Fragment

Al crear un fragment se tiene que respetar la siguiente nomenclatura para la clase `NombreFragment`
cada clase fragment tiene que terminar con la palabra fragment recuerda crear que el nombre tiene
que ser corto y especifico a la función que se le dará al fragment por ejemplo:

- ToolFragment
- ToolListFragment
- ToolUpdateFragment

Recuerda crear un paquete que encierre todo los fragments.

### Base Fragment

* Implementaremos el viewModel y el DataBinding, y pasaremos como parametro el layout.

* Se creara y bindeara automaticamente a tu xml.

* Para usar las funciones correctamente solo debes acceder a `onCreateConfig` este tomara el lugar
  de `onCreateView` o `onViewCreated`

```kt
abstract class BaseFragment<ViewModel  : BaseViewModel, DB : ViewDataBinding>(@LayoutRes val layout: Int) : Fragment() {

    protected abstract val viewModel: ViewModel
    open lateinit var binding: DB
   // lateinit var dataBindingComponent: DataBindingComponent
    private fun onCreateConfig(inflater: LayoutInflater, container: ViewGroup) {
        binding = DataBindingUtil.inflate(inflater, layout, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.setVariable(BR.viewModel, viewModel)
    }

    open fun onCreateConfig() {}


    open fun onInject() {}

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        onCreateConfig(inflater, container!!)
        onCreateConfig()
        super.onCreateView(inflater, container, savedInstanceState)
        return binding.root
    }

    open fun refresh() {}

    open fun navigate(action: Int) {
        view?.let { _view ->
            Navigation.findNavController(_view).navigate(action)
        }
    }
}
```

## Background

Se encontrara en Model esta carpeta se usará para el manejo de hilos, por ejemplo:

- Handler
- Thread
- Corrutina
- AsyncTask (Deprecado)
- Wormanager
- LiveData
- MutableData
- Flow
- suspendCoroutienesScope
- Job y mas.

La declaración de la clase será la siguiente `NombreMetodo` cada clase tendrá que terminar la
declaración del método usado, por ejemplo:

- SynchronizeUserHandler
- SynchronizeUserThread
- SynchronizeUserCorrutina
- SynchronizeUserAsyncTask (Deprecado)
- SynchronizeUserWormanager

Ejemplo de clase con Thread:

```sh
class SynchronizeThread {

    fun synchronizeUser() {
        Thread {

        }.start()
    }

    fun synchronizeTool() {
        Thread {

        }.start()
    }
}
```

## Extension

En la carpeta extension se guardara el archivo .kt llamado ExtensionApp que permitirá usar métodos
heredados teste las clases como String,int,etc.

* Si estas extension se usaran en mas de 1 modulo, se recomienda ponerlo en el modulo Model.
* Si estas extension solo se usan en la capa de UI, se recomienda ponerlo en los Utils UI.

Ejemplo de ExtensionApp:

  ```sh
fun String.verifyEmail(): String {
    return ""
}
```

## ViewModel

Para usar la carpeta viewModel primero se tiene que utilizar las librerías de Android para el mvvm
que son las siguientes:

 ```sh
    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:2.2.0"
    implementation "androidx.lifecycle:lifecycle-livedata-ktx:2.2.0"
```

Una vez implementadas se usará el método de dataBinding para configúralo se realiza en el mismo
gradle de la siguiente manera:

 ```sh
    buildFeatures{
        dataBinding = true
    }
```

Ahora al crear las clases la nomenclatura es la siguiente el nombreActivityViewModel usar el nombre
del activity y terminando con el viewmodel debito que haga se implementara la lógica de negocio

Ejemplo de clase viewmodel:

 ```kt
class SplashFragment :
    BaseFragment<SplashFragmentViewModel, FragmentSplashBinding>{

    }
```

Ahora probaremos el uso de viewModel con DataBinding.

* Aclararemos que no se usa nada de configuracion adicional a la mostrada en el KT del splash
  unicamente la inicializacion del DataBinding con el ViewModel.

* Vemos que cuando koin inicializa el viewModel ejecuta la funcion `settingsDashboardFirstTimeOrNot`
  que cambia los valores de  `visibilityButtonExplore` y  `navigateDashboard`.

 ```kt
class SplashFragmentViewModel(var storeManager: DataStoreSource) : BaseViewModel() {
    var visibilityButtonExplore = MutableLiveData<Boolean>()
    var navigateDashboard = MutableLiveData<Boolean>()

    init {
        settingsDashboardFirstTimeOrNot()
    }

    private fun settingsDashboardFirstTimeOrNot() {
        Coroutines.io {
            if (storeManager.getStringDataStore(Constants.Coords.LON).isNullOrEmpty()) {
                navigateDashboard.postValue(false)
                visibilityButtonExplore.postValue(true)
            } else {
                navigateDashboard.postValue(true)
                visibilityButtonExplore.postValue(false)
            }
        }
    }
}
 ```

* En el xml podemos leer los `mutableLiveData` :

Declaramos el viewModel.

 ```xml

<data>

    <variable name="viewModel"
        type="com.doapps.android.weatherapp.ui.splash.SplashFragmentViewModel" />
</data>
 ```

Accedemos al valor de la variable `mutableLiveData`.

 ```xml

<com.google.android.material.button.MaterialButton android:id="@+id/buttonExplore"
    android:layout_width="wrap_content" android:layout_height="60dp"
    android:layout_alignParentBottom="true" android:layout_centerHorizontal="true"
    android:layout_marginBottom="30dp" android:elevation="2dp" android:gravity="center"
    android:paddingStart="60dp" android:paddingEnd="60dp" android:text="@string/explore"
    android:textAllCaps="true" android:textAppearance="@style/AppTheme"
    android:textColor="@android:color/black" android:textSize="16sp" android:textStyle="bold"
    app:visibility="@{viewModel.visibilityButtonExplore.booleanValue()}"
    app:backgroundTint="@color/white" app:cornerRadius="24dp" />
 ```

# Libraries

<li><a href="https://mockk.io/">Mockk</a></li>
<li><a href="https://junit.org/junit4/">Junit</a></li>
<li><a href="http://robolectric.org/">RoboElectric</a></li>
<li><a href="https://github.com/lopspower/RxAnimationr">Rx Animation </a></li>
<li><a href="https://developer.android.com/topic/libraries/architecture/navigationr">Navigation Components </a></li>
<li><a href="https://square.github.io/retrofit/">Retrofit</a></li>
<li><a href="https://developer.android.com/topic/libraries/architecture/room">Room</a></li>
<li><a href="https://developer.android.com/topic/libraries/architecture/datastore">DataStore</a></li>
<li><a href="https://github.com/loopeer/shadow">Shadow</a></li>
<li><a href="https://www.bugsnag.com/">Bugsnack</a></li>
<li><a href="https://github.com/JakeWharton/ThreeTenABP">threetenabp</a></li>
<li><a href="https://github.com/square/retrofit/tree/master/retrofit-converters/gson">GsonConverter</a></li>
<li><a href="https://github.com/algolia/algoliasearch-client-android">algoliasearch</a></li>
<li><a href="https://developer.android.com/topic/libraries/data-binding">DataBinding</a></li>




