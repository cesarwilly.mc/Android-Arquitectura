package com.doapps.android.domain

import android.os.Build
import com.doapps.android.domain.repo.CurrentWeatherRepositoryTest
import com.doapps.android.domain.repo.ForecastRepositoryTest
import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.robolectric.annotation.Config



@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(Suite::class)
@Suite.SuiteClasses(
    ForecastRepositoryTest::class,
    CurrentWeatherRepositoryTest::class
)
class TestSuite
