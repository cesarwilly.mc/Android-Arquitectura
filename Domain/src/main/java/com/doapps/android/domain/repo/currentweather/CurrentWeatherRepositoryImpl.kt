package com.doapps.android.domain.repo.currentweather

import NetworkBoundResource
import androidx.lifecycle.LiveData
import com.doapps.android.model.utils.conexion.SafeApiRequest
import com.doapps.android.model.general.CurrentWeatherResponse
import com.doapps.android.model.room.CurrentWeatherEntity
import com.doapps.android.conexionmodule.datasource.currentWeather.CurrentWeatherLocalDataSource
import com.doapps.android.conexionmodule.datasource.currentWeather.CurrentWeatherRemoteDataSource
import com.doapps.android.model.utils.Constants.NetworkService.RATE_LIMITER_TYPE
import com.doapps.android.model.utils.RateLimiter
import com.doapps.android.model.utils.status.Resource

import java.util.concurrent.TimeUnit


class CurrentWeatherRepositoryImpl(
    private val currentWeatherRemoteDataSource: CurrentWeatherRemoteDataSource,
    private val currentWeatherLocalDataSource: CurrentWeatherLocalDataSource
) : SafeApiRequest(), CurrentWeatherRepository {

    override val currentWeatherRateLimit =
        RateLimiter<String>(30, TimeUnit.SECONDS)

    override fun loadCurrentWeatherByGeoCords(
        lat: Double,
        lon: Double,
        fetchRequired: Boolean,
        units: String
    ): LiveData<Resource<CurrentWeatherEntity>> {
        return object : NetworkBoundResource<CurrentWeatherEntity, CurrentWeatherResponse>() {
            override suspend fun saveCallResult(item: CurrentWeatherResponse) =
                currentWeatherLocalDataSource.insertCurrentWeather(item)

            override fun shouldFetch(data: CurrentWeatherEntity?): Boolean = fetchRequired

            override fun loadFromDb(): LiveData<CurrentWeatherEntity> =
                currentWeatherLocalDataSource.getCurrentWeather()

            override suspend fun createCall(): CurrentWeatherResponse = apiRequest {
                currentWeatherRemoteDataSource.getCurrentWeatherByGeoCords(
                    lat,
                    lon,
                    units
                )
            }

            override fun onFetchFailed() = currentWeatherRateLimit.reset(RATE_LIMITER_TYPE)
        }.asLiveData
    }

    override suspend fun getCurrentWeatherByGeoCords(lat: Double, lon: Double, metric: String) =
        currentWeatherRemoteDataSource.getCurrentWeatherByGeoCords(lat, lon, metric)

    override suspend fun insertCurrentWeather(weatherResponse: CurrentWeatherResponse) =
        currentWeatherLocalDataSource.insertCurrentWeather(weatherResponse)

    override fun getCurrentWeather() = currentWeatherLocalDataSource.getCurrentWeather()
}
