package com.doapps.android.domain.repo.dataStore

import kotlinx.coroutines.flow.Flow



interface DataStoreRepository {

    fun getStringDataStoreFlow(key: String): Flow<String>

    suspend fun getStringDataStore(key: String): String

    suspend fun setStringDataStore(key: String, value: String)


    suspend fun deleteAllData()

}