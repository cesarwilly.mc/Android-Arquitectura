package com.doapps.android.domain.repo.searchcities

import androidx.lifecycle.LiveData
import com.doapps.android.model.room.CitiesForSearchEntity
import com.doapps.android.model.utils.RateLimiter
import com.doapps.android.model.utils.status.Resource

interface SearchCitiesRepository {

    val rateLimiter: RateLimiter<String>

    fun loadCitiesByCityName(cityName: String?): LiveData<Resource<List<CitiesForSearchEntity>>>

}