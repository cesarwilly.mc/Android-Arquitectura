package com.doapps.android.domain.repo.dataStore

import com.doapps.android.conexionmodule.datasource.dataStore.DataStoreDataSource
import com.doapps.android.conexionmodule.db.dataStore.DataStoreManager
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first

class DataStoreRepositoryImpl(private val dataStoreManager: DataStoreDataSource):DataStoreRepository {


    override fun getStringDataStoreFlow(key: String): Flow<String> = dataStoreManager.getStringDataStoreFlow(key)

    override suspend fun getStringDataStore(key: String)  = dataStoreManager.getStringDataStore(key)

    override suspend fun setStringDataStore(key: String, value: String) = dataStoreManager.setStringDataStore(key, value)

    override suspend fun deleteAllData() = dataStoreManager.deleteAllData()

}