package com.doapps.android.domain.repo.forecast

import NetworkBoundResource
import androidx.lifecycle.LiveData
import com.doapps.android.model.utils.conexion.SafeApiRequest
import com.doapps.android.model.general.ForecastResponse
import com.doapps.android.model.room.ForecastEntity
import com.doapps.android.conexionmodule.datasource.forecast.ForecastLocalDataSource
import com.doapps.android.conexionmodule.datasource.forecast.ForecastRemoteDataSource
import com.doapps.android.model.utils.Constants.NetworkService.RATE_LIMITER_TYPE
import com.doapps.android.model.utils.RateLimiter
import com.doapps.android.model.utils.status.Resource
import java.util.concurrent.TimeUnit


class ForecastRepositoryImpl(
    private val forecastRemoteDataSource: ForecastRemoteDataSource,
    private val forecastLocalDataSource: ForecastLocalDataSource
) : SafeApiRequest(), ForecastRepository {

    override val forecastListRateLimit =
        RateLimiter<String>(30, TimeUnit.SECONDS)

    override fun loadForecastByCoord(
        lat: Double,
        lon: Double,
        fetchRequired: Boolean,
        units: String
    ): LiveData<Resource<ForecastEntity>> {
        return object : NetworkBoundResource<ForecastEntity, ForecastResponse>() {
            override suspend fun saveCallResult(item: ForecastResponse) =
                forecastLocalDataSource.insertForecast(item)

            override fun shouldFetch(data: ForecastEntity?): Boolean = fetchRequired

            override fun loadFromDb(): LiveData<ForecastEntity> =
                forecastLocalDataSource.getForecast()

            override suspend fun createCall(): ForecastResponse =
                apiRequest { forecastRemoteDataSource.getForecastByGeoCords(lat, lon, units) }

            override fun onFetchFailed() = forecastListRateLimit.reset(RATE_LIMITER_TYPE)
        }.asLiveData
    }

    override fun getForecast() = forecastLocalDataSource.getForecast()

    override fun insertForecast(forecast: ForecastResponse) =
        forecastLocalDataSource.insertForecast(forecast)

    override suspend fun getForecastByGeoCords(lat: Double, lon: Double, metric: String) = forecastRemoteDataSource.getForecastByGeoCords(lat,lon,metric)
}
