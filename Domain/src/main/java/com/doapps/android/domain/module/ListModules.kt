package com.doapps.android.domain.module

import com.doapps.android.conexionmodule.module.dataSourceModule
import com.doapps.android.conexionmodule.module.databaseModule
import com.doapps.android.conexionmodule.module.networkModule

val listOfModulesDomain = listOf( repositoryModule, useCaseModule)
val databaseModuleData= databaseModule
val networkModuleData= networkModule
val dataSourceModuleData= dataSourceModule