package com.doapps.android.domain.repo.currentweather

import androidx.lifecycle.LiveData
import com.doapps.android.model.general.CurrentWeatherResponse
import com.doapps.android.model.room.CurrentWeatherEntity
import com.doapps.android.model.utils.RateLimiter
import com.doapps.android.model.utils.status.Resource
import retrofit2.Response

interface CurrentWeatherRepository {
    val currentWeatherRateLimit: RateLimiter<String>
    fun loadCurrentWeatherByGeoCords(
        lat: Double,
        lon: Double,
        fetchRequired: Boolean,
        units: String
    ): LiveData<Resource<CurrentWeatherEntity>>

    suspend fun getCurrentWeatherByGeoCords(lat: Double, lon: Double, metric: String): Response<CurrentWeatherResponse>
    suspend fun insertCurrentWeather(createSampleCurrentWeatherResponse: CurrentWeatherResponse)
    fun getCurrentWeather(): LiveData<CurrentWeatherEntity>
}