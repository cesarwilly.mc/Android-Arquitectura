package com.doapps.android.domain.repo.forecast

import androidx.lifecycle.LiveData
import com.doapps.android.model.general.ForecastResponse
import com.doapps.android.model.room.ForecastEntity
import com.doapps.android.model.utils.RateLimiter
import com.doapps.android.model.utils.status.Resource
import retrofit2.Response

interface ForecastRepository {

    val forecastListRateLimit: RateLimiter<String>
    fun loadForecastByCoord(
        lat: Double,
        lon: Double,
        fetchRequired: Boolean,
        units: String
    ): LiveData<Resource<ForecastEntity>>

    fun getForecast(): LiveData<ForecastEntity>

    fun insertForecast(forecast: ForecastResponse)

    suspend fun getForecastByGeoCords(
        lat: Double,
        lon: Double,
        metric: String
    ): Response<ForecastResponse>
}